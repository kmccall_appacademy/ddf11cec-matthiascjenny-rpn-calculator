class RPNCalculator
  attr_accessor :calculator

  def initialize
    @calculator = []
  end

#  def initialize(calculator)
#    @calculator = calculator
#  end

  def push(num)
    @calculator = calculator.push(num)
  end

  def plus
    if @calculator.empty?
      raise "calculator is empty"
    else
      @calculator[-2] = calculator[-2] + calculator[-1]
      calculator.pop
    end
  end

  def minus
    if @calculator.empty?
      raise "calculator is empty"
    else
      @calculator[-2] = calculator[-2] - calculator[-1]
      calculator.pop
    end
  end

  def times
    if @calculator.empty?
      raise "calculator is empty"
    else
      @calculator[-2] = calculator[-2] * calculator[-1]
      calculator.pop
    end
  end

  def divide
    if @calculator.empty?
      raise "calculator is empty"
    else
      @calculator[-2] = calculator[-2].to_f / calculator[-1]
      calculator.pop
    end
  end

  def value
    return calculator[-1]
  end

  def tokens(str)
    result = []
    arr = str.split(" ")
    arr.each do |ch|
      if ("0".."9").include?(ch)
        result.push(ch.to_i)
      elsif ch == "+"
        result.push(:+)
      elsif ch == "-"
        result.push(:-)
      elsif ch == "*"
        result.push(:*)
      else
        result.push(:/)
      end
    end
    return result
  end

  def evaluate(str)
    tokenized = tokens(str)
    tokenized.each do |ch|
      if (0..9).include?(ch)
        self.push(ch)
      elsif ch == :+
        self.plus
      elsif ch == :-
        self.minus
      elsif ch == :*
        self.times
      else
        self.divide
      end
    end
    self.value
  end
end
